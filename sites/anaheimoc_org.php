<?php

function submit_anaheimoc_org(Zend_Http_Client $client, $fields) {
	// Open up submission form
	$anaheimoc_url='http://anaheimoc.org/calendar-of-events/submit';
	showlog('Opening form: %s', $anaheimoc_url);
	$client->setUri($anaheimoc_url);
	$response=$client->request('GET');
	if(!$response->isSuccessful()) {
		throw new Zend_Http_Exception($response->responseCodeAsText());
	}
	$doc=phpQuery::newDocumentHTML($response->getBody());
	$form_fields=$doc->find('form.webform-client-form')->serializeArray();

	$data=array();
	foreach($form_fields as $field) {
		if(strlen($field['value'])>0)
			$data[$field['name']]=$field['value'];
	}

	$fields_map = array(
		'submitted[event_name]' => 'event_name',
		'submitted[event_start_date][month]' => 'event_start_month',
		'submitted[event_start_date][day]' => 'event_start_day',
		'submitted[event_start_date][year]' => 'event_start_year',
		'submitted[event_start_time][hour]' => 'event_start_hour',
		'submitted[event_start_time][minute]' => 'event_start_minute',
		'submitted[event_start_time][ampm]' => 'event_start_ampm',
		'submitted[event_end_date][month]' => 'event_end_month',
		'submitted[event_end_date][day]' => 'event_end_day',
		'submitted[event_end_date][year]' => 'event_end_year',
		'submitted[event_end_time][hour]' => 'event_end_hour',
		'submitted[event_end_time][minute]' => 'event_end_minute',
		'submitted[event_end_time][ampm]' => 'event_end_ampm',
		'submitted[event_description]' => 'event_description',
		'submitted[event_location_name]' => 'event_location_name',
		'submitted[event_location_address]' => 'event_location_address',
		'submitted[event_location_city]' => 'event_location_city',
		'submitted[event_location_state]' => 'event_location_state',
		'submitted[event_location_zip_code]' => 'event_location_zip',
		'submitted[event_website]' => 'event_website',
		'submitted[event_cost]' => 'event_cost',
		'submitted[contact_information][name_first]' => 'first_name',
		'submitted[contact_information][name_last]' => 'last_name', 
		'submitted[contact_information][email]' => 'email',
		'submitted[contact_information][phone]' => 'phone',
		'submitted[event_phone]' => 'phone'
	);
	foreach($fields_map as $k => $v) {
		if(isset($fields[$v]) && strlen($fields[$v]) > 0) {
			$data[$k]=$fields[$v];
		} else {
			unset($data[$k]);
		}
	}



	$client->setUri('http://anaheimoc.org/calendar-of-events/submit');
	$client->setParameterPost($data);
	$response=$client->request('POST');
	if(!$response->isSuccessful()) {
		throw new Zend_Http_Exception($response->responseCodeAsText());
	}
	$doc=phpQuery::newDocumentHTML($response->getBody());
	$error_msg=$doc->find('div.messages.error');
	if($error_msg->count()) {
		throw new Form_Error_Exception($error_msg->html());
		return;
	}
	echo '<p style="color:green">Success!</p>';
	$confirm_msg=$doc->find('div.webform-confirmation');
	if($confirm_msg->count()) {
		echo $confirm_msg->html();
	}
}