<?php

function submit_ocmommag_com(Zend_Http_Client $client, $fields) {
	// Open login page
	$url='http://www.ocmommag.com/main/authorization/signIn?target=http%3A%2F%2Fwww.ocmommag.com%2F';
	showlog('Opening login page: %s', $url);
	$client->setUri($url);
	$response=$client->request('GET');
	if(!$response->isSuccessful()) {
		throw new Zend_Http_Exception($response->responseCodeAsText());
	}
	$doc=phpQuery::newDocumentHTML($response->getBody());
	$form_fields=$doc->find('form#event_form')->serializeArray();
	$data=array();
	foreach($form_fields as $field) {
		if(strlen($field['value'])>0)
			$data[$field['name']]=$field['value'];
	}
	$data['emailAddress']='Kenny@truckedupmarketing.com';
	$data['password']='xYH23Fqpz';
	$url='http://www.ocmommag.com/main/authorization/doSignIn?target=http%3A%2F%2Fwww.ocmommag.com%2F';
	showlog('Submitting login information: %s', $url);
	$client->setUri($url);
	$client->setParameterPost($data);
	$response=$client->request('POST');
	if(!$response->isSuccessful()) {
		throw new Zend_Http_Exception($response->responseCodeAsText());
	}
	$doc=phpQuery::newDocumentHTML($response->getBody());
	$error_msg=$doc->find('div.dy-error-msg');
	if($error_msg->count() > 0 && $error_msg->is(':visible')) {
		throw new Exception($error_msg->html());
	}
	// Let's go to submission form
	$url='http://www.ocmommag.com/events/event/new?cancelTarget=http%3A%2F%2Fwww.ocmommag.com%2Fevents%2F';
	showlog('Login succesful, opening form: %s', $url);
	$client->setUri($url);
	$response=$client->request('GET');
	if(!$response->isSuccessful()) {
		throw new Zend_Http_Exception($response->responseCodeAsText());
	}
	$doc=phpQuery::newDocumentHTML($response->getBody());
	$form_fields=$doc->find('form#event_form')->serializeArray();

	$data=array();
	foreach($form_fields as $field) {
		if(strlen($field['value'])>0)
			$data[$field['name']]=$field['value'];
	}
	
	$fields_map = array(
		'title' => 'event_name',
		'description' => 'event_description',
		'start' => function($fields) {
			return sprintf('%02d-%02d-%4d',$fields['event_start_month'],$fields['event_start_day'],$fields['event_start_year']);
		},
		'end' => function($fields) {
			return sprintf('%02d-%02d-%4d',$fields['event_end_month'],$fields['event_end_day'],$fields['event_end_year']);
		},
		'startTimeH' => 'event_start_hour',
		'startTimeI' => 'event_start_minute',
		'startTimeR' => 'event_start_ampm',
		'endTimeH' => 'event_end_hour',
		'endTimeI' => 'event_end_minute',
		'endTimeR' => 'event_end_ampm',
		'location' => 'event_location_name',
		'street' => 'event_location_address',
		'city' => function($fields) {
			return $fields['event_location_city'].', '.$fields['event_location_state'];
		},
		'type' => 'event_type',
		'website' => 'event_website',
		'contact' => 'phone',
		'organizedBy' => function($fields) {
			return $fields['first_name'].' '.$fields['last_name'];
		}
	);
	foreach($fields_map as $k => $v) {
		if($v instanceof Closure) {
			$data[$k]=$v($fields);
		} elseif(is_string($v)) {
			if(isset($fields[$v]) && strlen($fields[$v]) > 0) {
				$data[$k]=$fields[$v];
			} else {
				unset($data[$k]);
			}
		}
	}
	
	
	$client->setUri('http://www.ocmommag.com/events/event/create');
	$client->setParameterPost($data);
	$response=$client->request('POST');
	if(!$response->isSuccessful()) {
		throw new Zend_Http_Exception($response->responseCodeAsText());
	}
//	echo $response->getBody();exit;
	$doc=phpQuery::newDocumentHTML($response->getBody());
	$error_msg=$doc->find('form div.errordesc');
//	echo $error_msg->html();exit;
	if($error_msg->is(':visible')) {
		throw new Form_Error_Exception($error_msg->html());
	}
//	die('ngok');
	echo '<p style="color:green">Success!</p>';
	$confirm_msg=$doc->find('div.webform-confirmation');
	if($confirm_msg->count()) {
		echo $confirm_msg->html();
	}
	//errordesc msg clear
}

