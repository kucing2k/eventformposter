<?php

$base_dir=dirname(__FILE__);
$include_paths=array(
	$base_dir.'/vendors',
	$base_dir.'/vendors/ZendFramework'
);
set_include_path(get_include_path().PATH_SEPARATOR.join(PATH_SEPARATOR, $include_paths));

// Load libraries
require_once 'Zend/Loader/Autoloader.php';
require_once 'phpQuery/phpQuery.php';
spl_autoload_register(array('Zend_Loader_Autoloader','autoload'));

// Gather all vars
$fields = array(
	'event_name' => '%s',
	'event_start_month' => '%d',
	'event_start_day' => '%d',
	'event_start_year' => '%d',
	'event_start_hour' => '%d',
	'event_start_minute' => '%d',
	'event_start_ampm' => '%s',
	'event_end_month' => '%d',
	'event_end_day' => '%d',
	'event_end_year' => '%d',
	'event_end_hour' => '%d',
	'event_end_minute' => '%d',
	'event_end_ampm' => '%s',
	'event_description' => '%s',
	'event_type' => '%s',
	'event_location_name' => '%s',
	'event_location_address' => '%s',
	'event_location_city' => '%s',
	'event_location_state' => '%s',
	'event_location_zip' => '%s',
	'event_website' => '%s',
	'event_cost' => '%s',
	'first_name' => '%s',
	'last_name' => '%s', 
	'email' => '%s',
	'phone' => '%s',
);

$sites = array(
	'anaheimoc_org',
	'ocmommag_com',
);

$client=new Zend_Http_Client();
$client->setCookieJar();
$client->setConfig(array(
	'adapter'=>'Zend_Http_Client_Adapter_Socket',
	'useragent'=>$user_agents[mt_rand(0, count($user_agents)-1)],
	'timeout'=>10
));

$user_agents = array(
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.3) Gecko/20090913 Firefox/3.5.3",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 3.5.30729)",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 3.5.30729)",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.1) Gecko/20090718 Firefox/3.5.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.1 (KHTML, like Gecko) Chrome/4.0.219.6 Safari/532.1",
	"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; InfoPath.2)",
	"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; SLCC1; .NET CLR 2.0.50727; .NET CLR 1.1.4322; .NET CLR 3.5.30729; .NET CLR 3.0.30729)",
	"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; Win64; x64; Trident/4.0)",
	"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; SV1; .NET CLR 2.0.50727; InfoPath.2)Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)",
	"Mozilla/4.0 (compatible; MSIE 6.1; Windows XP)"
);

foreach($fields as $var => $format) {
	$fields[$var]=(isset($_POST[$var]) && strlen($_POST[$var]))
		? sprintf($format,$_POST[$var])
		: '';
}

$doc = phpQuery::newDocumentFile($base_dir.'/form.html');

if(empty($_POST)) {
	echo $doc->html();
	exit;
}

$form = $doc->find('form');
foreach($fields as $k => $v) {
	$input=$form->find('[name="'.$k.'"]');
	if($input->is('input[type="radio"]') || $input->is('input[type="checkbox"]')) {
		$cbr=$input->filter('[value="'.$v.'"]');
		$cbr->attr('checked','checked');
	} else {
		$input->val($v);
	}
}


foreach($sites as $site) {
	showlog("Proceeding with %s",$site);
	$file=$base_dir.'/sites/'.$site.'.php';
	$func='submit_'.$site;
	try {
		if(!file_exists($file))
			throw new Exception(sprintf('File %s not found',$file));
		require_once $file;
		call_user_func_array($func, array($client, $fields));
	} catch (Exception $e) {
		$err=sprintf('Exception "%s": %s\n<blockquote><pre>%s</pre></blockquote>',get_class($e),$e->getMessage(),$e->getTraceAsString());
		$msg_box=$doc->find('div.error-message');
		$msg_box->removeClass('hide');
		$msg_box->html($err);
		echo $doc->html();
		exit;
	}
}

function showlog($msg) {
	echo nl2br(call_user_func_array('sprintf', func_get_args()) . "\n");
}

class Form_Error_Exception extends Exception {}
